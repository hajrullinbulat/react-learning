import React, {Component} from 'react';
import Aux from "../../../hoc/Aux/Aux";
import Button from "../../UI/Button/Button";

class OrderSummary extends Component {
  render() {
    const ingredientSummary = Object.keys(this.props.ingredients)
      .map(igKey => (
        <li key={igKey}>
          <span style={{textTransform: 'capitalize'}}>{igKey}</span>: {this.props.ingredients[igKey]}
        </li>
      ));

    return (
      <Aux>
        <h3>Your order</h3>
        <p>Following ingredients</p>
        <ul>
          {ingredientSummary}
        </ul>
        <p>Total Price: <strong>{this.props.price.toFixed(2)}</strong></p>
        <p>Continue to Checkout?</p>
        <Button btnType='Danger' clicked={this.props.canceled}>CANCEL</Button>
        <Button btnType='Success' clicked={this.props.continued}>CONTINUE</Button>
      </Aux>
    );
  }
}

export default OrderSummary;