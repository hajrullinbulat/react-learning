import React, {Component} from 'react';
import CheckoutSummary from "../../components/Order/CheckoutSummary/CheckoutSummary";
import Route from "react-router-dom/es/Route";
import ContactData from "./ContactData/ContactData"

class Checkout extends Component {
  state = {
    ingredients: null,
    totalPrice: 0
  };
  checkoutCanceled = () => {
    this.props.history.goBack();
  };
  checkoutContinued = () => {
    this.props.history.replace('/checkout/contact-data')
  };

  componentWillMount() {
    const query = new URLSearchParams(this.props.location.search);
    const ing = {};
    let price = 0;
    for (let param of query.entries()) {
      if (param[0] === 'price') {
        price = param[1]
      } else {
        ing[param[0]] = +param[1]
      }
    }
    this.setState({ingredients: ing, totalPrice: price})
  }

  render() {
    return (
      <div>
        <CheckoutSummary
          ingredients={this.state.ingredients}
          canceled={this.checkoutCanceled}
          continued={this.checkoutContinued}
        />
        <Route
          path={this.props.match.path + '/contact-data'}
          render={(props) => (<ContactData ingredients={this.state.ingredients} price={this.state.totalPrice} {...props}/>)}/>
      </div>
    );
  }
}

export default Checkout;